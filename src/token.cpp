#include "token.h"

Token::Token(TOKEN_TYPE type, int line, std::string lexeme) {
  this->type = type;
  this->line = line;
  this->lexeme = lexeme;
}

TOKEN_TYPE Token::getType() const {
  return type;
}

std::string Token::getLexeme() const {
  return lexeme;
}

unsigned int Token::getLine() const {
  return line;
}

std::string Token::getNameFromType(TOKEN_TYPE type) {
  for (auto kv : keywords) {
    if (kv.second == type) {
      return kv.first;
    }
  }
  return "";
}

const std::unordered_map<std::string, TOKEN_TYPE> Token::keywords =
{
  {"int", INT},
  {"char", CHAR},
  {"void", VOID},
  {"[", LBRACK},
  {"]", RBRACK},
  {"{", LBRACE},
  {"}", RBRACE},
  {"(", LPAREN},
  {")", RPAREN},
  {",", COMMA},
  {";", SEMI},
  {"=", EQUAL},
  {"==", EQUAL_EQUAL},
  {"+", ADD},
  {"-", SUBTRACT},
  {"*", STAR},
  {"/", DIVIDE},
  {">", GREATER_THAN},
  {"<", LESS_THAN},
  {"&&", AND},
  {"||", OR},
  {"!", NOT},
  {"extern", EXTERN},
  {"if", IF},
  {"else", ELSE},
  {"while", WHILE},
  {"return", RETURN},
};
