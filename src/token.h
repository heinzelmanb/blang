#pragma once
#include <string>
#include <memory>
#include <unordered_map>

class Token;

typedef std::shared_ptr<Token> SToken; typedef std::unique_ptr<Token> UToken;
enum TOKEN_TYPE {
  ID,
  INT,
  INT_VAL,
  CHAR_VAL,
  STRING_VAL,
  CHAR, 
  VOID,
  LBRACK,
  RBRACK,
  LPAREN,
  RPAREN,
  LBRACE,
  RBRACE,
  COMMA,
  SEMI,
  EQUAL,
  EQUAL_EQUAL,
  ADD,
  SUBTRACT,
  STAR,
  DIVIDE,
  GREATER_THAN,
  LESS_THAN,
  AND,
  OR,
  NOT,
  NOT_EQUAL,
  IF,
  ELSE,
  WHILE,
  RETURN,
  EXTERN,
  END,
  ERROR
};

class Token {
  public:
	Token(TOKEN_TYPE type, int line, std::string lexeme="");

	TOKEN_TYPE getType() const;

	std::string getLexeme() const;

	unsigned int getLine() const;
	
	static const std::unordered_map<std::string, TOKEN_TYPE> keywords;
	static std::string getNameFromType(TOKEN_TYPE type);
  private:
	TOKEN_TYPE type;
	std::string lexeme;
	unsigned int line;
};
