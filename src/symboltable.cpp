#include "symboltable.h"
#include "blang_exception.h"

using namespace ast;

SymbolTable::SymbolTable() 
{
}

void SymbolTable::addVariableSymbol(std::string id, SType type) {
  if (getVariableType(id)) {
    std::string err = id + " already defined";
    throw BlangException(err.c_str());
  }
  variables[variables.size() - 1].insert(std::pair<std::string, SType>(id, type));
}

void SymbolTable::addFunctionSymbol(std::string id, SFunction function) { 
  if (getFunctionReturnType(id)) {
    std::string err = id + " already defined";
    throw BlangException(err.c_str());
  }
  functions.insert(std::pair<std::string, SFunction>(id, function));
}

void SymbolTable::pushStack() {
  variables.push_back(std::unordered_map<std::string, SType>());
}

void SymbolTable::popStack() {
  variables.pop_back();
}

SType SymbolTable::getVariableType(std::string id) const {
  for (int i = variables.size() - 1; i >= 0; i--) {
    auto it = variables[i].find(id);
    if (it != variables[i].end()) {
      return it->second;
    }
  }
  return nullptr;
}

SType SymbolTable::getFunctionReturnType(std::string id) const {
  auto it = functions.find(id);

  if (it != functions.end()) {
    return it->second->getType();
  }
  return nullptr;
}

std::vector<ast::SType> SymbolTable::getFunctionParamType(std::string id) const {
  auto it = functions.find(id);
  
  std::vector<ast::SType> paramList;

  if (it != functions.end()) {

    if (it->second->getParameters()) {
      auto params = it->second->getParameters()->getParameters(); 

      for (auto param : params) {
        paramList.push_back(param->getType());
      }
    }
  }

  return paramList;
}
