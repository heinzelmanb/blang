#pragma once

#include <memory>
#include <ostream>

#include "visitor.h"
#include "symboltable.h"
#include "ast.h"

class TypeCheck : public Visitor {
  public:
	TypeCheck(ast::SProgram program, std::shared_ptr<SymbolTable> symbols);

	~TypeCheck();
	
	/**
	 * Description: Performs a typecheck on the ast.
	 * This process will include typechecking expressions,
	 * typechecking function parameters, typechecking function return types
	 * and checking to see if variables that are referenced, and functions
	 * that are called are defined before use.
	 * Return: True if successful, false otherwise
	 */
	bool typeCheck(std::ostream& outputStream);
  

	/**
	 * Implements the visitor interface
	 */

	virtual void visit(ast::SProgram program);
	virtual void visit(ast::SFunctionList functionList);
	virtual void visit(ast::SFunction function);
	virtual void visit(ast::SExternFunction function);
	virtual void visit(ast::SFunctionImpl function);
	virtual void visit(ast::SStatementList statementList);
	virtual void visit(ast::SStatement statement);
	virtual void visit(ast::SAssignmentStatement asgnStatement);
	virtual void visit(ast::SAssignmentStatementTail asgnTail);
	virtual void visit(ast::SWhileStatement whileStmt);
	virtual void visit(ast::SIfStatement ifStmt);
	virtual void visit(ast::SElseStatement elseStmt);
	virtual void visit(ast::SReturnStatement returnStmt);
	virtual void visit(ast::SFunctionCallStatement funcCall);
	virtual void visit(ast::SType type);
	virtual void visit(ast::SParameterList params);
	virtual void visit(ast::SParameter param);
	virtual void visit(ast::SExpression exp);
	virtual void visit(ast::SIntExpression intExp);
	virtual void visit(ast::SNotExpression notExp);
	virtual void visit(ast::SCharLiteral charLit);
	virtual void visit(ast::SStringLiteral stringLit);
	virtual void visit(ast::SIntLiteral intLit);
	virtual void visit(ast::SIntId id);
	virtual void visit(ast::SIntFunctionCall functionCall);
	virtual void visit(ast::SComplexExpression complexExpr);
	virtual void visit(ast::SArguments args);
  private:
	// the program to type check
	ast::SProgram program;
  
	// symbol table
	std::shared_ptr<SymbolTable> symbols;
  
	// last type evaluated
	ast::SType lastType;

	// last return type
	ast::SType lastReturnType;
};
