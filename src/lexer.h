#pragma once
#include <fstream> 
#include <string>
#include <unordered_map>
#include <memory>
#include <ostream>

#include "token.h"

//forward dec
class Lexer;

typedef std::shared_ptr<Lexer> SLexer;
typedef std::unique_ptr<Lexer> ULexer;

/**
 * performs the lexical stage
 * of compilation
 */

class Lexer {
  public:
	Lexer(std::ifstream* program);
  
	/**
	 * Description: Returns the next
	 * token in the stream
	 */
	SToken nextToken();
	
	/**
	 * Description: Returns if there are
	 * any more tokens left to return.
	 */
	bool hasMore();
  
	/**
	 * Description: Outputs Tokens to the given output stream
	 */
	void outputTokenStream(std::ostream& out);

  private:
	std::ifstream* program;

	int lineNumber;
  
	/**
	 * Gets the next character,
	 * skips whitespace
	 */
	char nextChar();
	
	/**
	 * Gets the next character, including whitespace;
	 */
	char nextCharExplicit();
	
	/**
	 * Peeks the next character, will skip whitespace
	 */
	char peek();

	/**
	 * Peeks the next character, will NOT skip whitespace
	 */
	char peekExplicit();

	SToken getTextToken(std::string in);

	SToken getOtherToken(std::string in);
};
