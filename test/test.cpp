#include "../src/token.h" 
#include "../src/lexer.h"
#include "../src/parser.h"
#include "../src/ast.h"
#include "../src/typecheck.h"
#include "../src/symboltable.h"
#include "../src/blang_exception.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <fstream>
#include <ostream>
#include <iostream>

/**
 * tests if the lexer can recognize the tokens
 */
TEST_CASE("Lexer should recoginize tokens", "[Lexer]") {
  std::ifstream stream("test_files/lex_test.txt", std::ios::binary);
  auto lex = std::make_shared<Lexer>(Lexer(&stream));
  
  if (!lex) {
    std::cout << "NOT GOOD" << std::endl;
  }

  REQUIRE(lex->nextToken()->getType() == INT_VAL);
  REQUIRE(lex->nextToken()->getType() == INT_VAL);
  REQUIRE(lex->nextToken()->getType() == INT_VAL);
  REQUIRE(lex->nextToken()->getType() == ID);
  REQUIRE(lex->nextToken()->getType() == INT);
  REQUIRE(lex->nextToken()->getType() == CHAR);
  REQUIRE(lex->nextToken()->getType() == CHAR_VAL);
  REQUIRE(lex->nextToken()->getType() == LBRACE);
  REQUIRE(lex->nextToken()->getType() == RBRACE);
  REQUIRE(lex->nextToken()->getType() == LBRACK);
  REQUIRE(lex->nextToken()->getType() == RBRACK);
  REQUIRE(lex->nextToken()->getType() == LPAREN);
  REQUIRE(lex->nextToken()->getType() == RPAREN);
  REQUIRE(lex->nextToken()->getType() == ADD);
  REQUIRE(lex->nextToken()->getType() == SUBTRACT);
  REQUIRE(lex->nextToken()->getType() == STAR);
  REQUIRE(lex->nextToken()->getType() == DIVIDE);
  REQUIRE(lex->nextToken()->getType() == GREATER_THAN);
  REQUIRE(lex->nextToken()->getType() == LESS_THAN);
  REQUIRE(lex->nextToken()->getType() == AND);
  REQUIRE(lex->nextToken()->getType() == OR);
  REQUIRE(lex->nextToken()->getType() == WHILE);
  REQUIRE(lex->nextToken()->getType() == IF);
  REQUIRE(lex->nextToken()->getType() == RETURN);
  REQUIRE(lex->nextToken()->getType() == NOT);
  REQUIRE(lex->nextToken()->getType() == VOID);
  REQUIRE(lex->nextToken()->getType() == EXTERN);
  REQUIRE(lex->nextToken()->getType() == STRING_VAL);
  REQUIRE(lex->nextToken()->getType() == EQUAL_EQUAL);
  REQUIRE(lex->nextToken()->getType() == NOT_EQUAL);
}

TEST_CASE("Parser Should Handle a function list", "[Parser]") {  
  std::ifstream stream("test_files/function_list.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0);
  auto ast = parser.parse(devNull);
  REQUIRE(ast);
}

TEST_CASE("Parser Should Handle the different types", "[Parser]") {  
  std::ifstream stream("test_files/types.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);
  std::ostream devNull(0);
  auto ast = parser.parse(devNull);
  REQUIRE(ast);
}

TEST_CASE("Parser Should Handle the different expressions", "[Parser]") {  
  std::ifstream stream("test_files/expressions.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);
  std::ostream devNull(0);
  auto ast = parser.parse(devNull);
  REQUIRE(ast);
}

TEST_CASE("Parser Should Handle if statements", "[Parser]") {  
  std::ifstream stream("test_files/if.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);
  std::ostream devNull(0);
  auto ast = parser.parse(devNull);
  REQUIRE(ast);
}

TEST_CASE("Parser Should Handle while statements", "[Parser]") {  
  std::ifstream stream("test_files/while.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);
  std::ostream devNull(0);
  auto ast = parser.parse(devNull);
  REQUIRE(ast);
}

TEST_CASE("Parser Should Handle return statements", "[Parser]") {  
  std::ifstream stream("test_files/return.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);
  std::ostream devNull(0);
  auto ast = parser.parse(devNull);
  REQUIRE(ast);
}

TEST_CASE("Type Checker should force assignments to have correct types", "[TypeCheck]") {  
  std::ifstream stream("test_files/asgn_type_check.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);
  
  std::ostream devNull(0); 
  
  auto ast = parser.parse(devNull);
  
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(true);
  } else {
    REQUIRE(false);
  }
}

TEST_CASE("Type Checker should force function args to have correct types", "[TypeCheck]") {  
  std::ifstream stream("test_files/arguments_type_check.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0); 

  auto ast = parser.parse(devNull);
  
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(true);
  } 
  else {
    REQUIRE(false);
  }  
}

TEST_CASE("Type Checker should not allow vars that are undefined to be referenced", "[TypeCheck]") {  
  std::ifstream stream("test_files/undef_var_type_check.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0); 

  auto ast = parser.parse(devNull);
   
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(false);
  } 
  else {
    REQUIRE(true);
  }    
}

TEST_CASE("Type Checker should not allow function that are undefined to be called", "[TypeCheck]") {  
  std::ifstream stream("test_files/undef_func_type_check.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0); 
  
  auto ast = parser.parse(devNull);
  
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(false);
  } 
  else {
    REQUIRE(true);
  }    
}

TEST_CASE("Type Checker should allow function to return a value that is not its' return type", "[TypeCheck]") {  
  std::ifstream stream("test_files/function_return_type_check.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0); 
  
  auto ast = parser.parse(devNull);
  
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(true);
  } 
  else {
    REQUIRE(false);
  }    
}

TEST_CASE("Type Checker should not allow function to return a value that is not its' return type", "[TypeCheck]") {  
  std::ifstream stream("test_files/function_return_type_check1.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0); 
  
  auto ast = parser.parse(devNull);
  
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(false);
  } 
  else {
    REQUIRE(true);
  }    
}

TEST_CASE("Type Checker should give error for non-void function without return type", "[TypeCheck]") {  
  std::ifstream stream("test_files/function_return_type_check2.txt", std::ios::binary); 
  auto lexer = std::make_shared<Lexer>(Lexer(&stream));
  auto parser = Parser(lexer);

  std::ostream devNull(0); 
  
  auto ast = parser.parse(devNull);
  
  auto symbols = std::make_shared<SymbolTable>(SymbolTable());
  auto tc = TypeCheck(ast, symbols);

  if (tc.typeCheck(devNull)) {
    REQUIRE(false);
  } 
  else {
    REQUIRE(true);
  }    
}
