#pragma once

#include <vector>
#include <unordered_map>
#include <memory>
#include <string>

#include "ast.h"

/**
 * Symbol table for TypeChecking phase of compilation
 */

class SymbolTable {
  public:
	SymbolTable();
	
	/**
	 * Description: Pushes a new stack frame onto the symbol table.
	 * This function should be called when entering a new stack frame such as
	 * a new function, if statement, while statement etc.
	 */
	void pushStack();
  
	/**
	 * Description: Pops stack from symbol table.
	 * Should be called after leaving a stack frame. Leaving a function, if
	 * statement etc.
	 */
	void popStack();
	
	/**
	 * Description: Defines a variable and its type in the current stack
	 * frame .
	 */
	void addVariableSymbol(std::string id, ast::SType type);
	
	/**
	 * Description: Defines a function, its return type, and parameter types 
	 * in the current stack frame .
	 */
	void addFunctionSymbol(std::string id, ast::SFunction function);
	
	/**
	 * Description: If a variable is defined, its type
	 */
	ast::SType getVariableType(std::string id) const;
	
	/**
	 * Description: If a function is defined, return its return type
	 */
	ast::SType getFunctionReturnType(std::string id) const;
  
	/**
	 * Description: If a function is defined, return the types for its
	 * parameters.
	 */
	std::vector<ast::SType> getFunctionParamType(std::string id) const;
  private:
	std::vector<std::unordered_map<std::string, ast::SType> > variables;

	std::unordered_map<std::string, ast::SFunction> functions;
};
