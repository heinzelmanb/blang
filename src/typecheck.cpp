#include "typecheck.h"
#include "blang_exception.h"

#include <iostream>

TypeCheck::TypeCheck(ast::SProgram program, std::shared_ptr<SymbolTable> symbols) {
  this->symbols = symbols;
  this->program = program;
  this->lastType = std::shared_ptr<ast::Type>(); 
  this->lastReturnType = nullptr;
}

TypeCheck::~TypeCheck()
{
}

bool TypeCheck::typeCheck(std::ostream& outputStream) {
  try {
    program->accept(this);
  }
  catch (TypeError e) {
    outputStream << e.what() << std::endl;
    return false;
  }
  return true;
}

void TypeCheck::visit(ast::SProgram program) {
  // push new scope for program
  symbols->pushStack();

  if (program->getFunctionList()) {
    program->getFunctionList()->accept(this);
  }

  symbols->popStack();
}

void TypeCheck::visit(ast::SFunctionList functionList) {
  for (auto function : functionList->getFunctions()) {
    // visit each function
    function->accept(this);
  }
}

void TypeCheck::visit(ast::SFunction function) {  
}

void TypeCheck::visit(ast::SExternFunction function) {
  lastReturnType = nullptr;

  // add the function name to the symbol table
  symbols->addFunctionSymbol(function->getId(), function);
  
  // create a new scope for the function
  symbols->pushStack();
  
  // visit the parameters
  if (function->getParameters()) {
    function->getParameters()->accept(this);
  }
  
  symbols->popStack();

  if (lastReturnType != nullptr) {
    if (!lastReturnType->equal(function->getType())) {
      std::string err = "Function " + function->getId() + " has an invalid return type";
      throw TypeError(err, function->getLine());
    }
  } 
  else if (function->getType()->getType() != VOID) {
    std::string err = "Non-void function " + function->getId() + " does not return a value";
    throw TypeError(err, function->getLine());
  }
}

void TypeCheck::visit(ast::SFunctionImpl function) {
  lastReturnType = nullptr;

  // add the function name to the symbol table
  symbols->addFunctionSymbol(function->getId(), function);
  
  // create a new scope for the function
  symbols->pushStack();
  
  // visit the parameters
  if (function->getParameters()) {
    function->getParameters()->accept(this);
  }
  
  // visit the function body
  if (function->getStmtList()) {
    function->getStmtList()->accept(this);
  }

  symbols->popStack();

  if (lastReturnType != nullptr) {
    if (!lastReturnType->equal(function->getType())) {
      std::string err = "Function " + function->getId() + " has an invalid return type";
      throw TypeError(err, function->getLine());
    }
  } 
  else if (function->getType()->getType() != VOID) {
    std::string err = "Non-void function " + function->getId() + " does not return a value";
    throw TypeError(err, function->getLine());
  }
}

void TypeCheck::visit(ast::SStatementList statementList) {
  for (auto stmt : statementList->getStatements()) {
    stmt->accept(this);
  }
}

void TypeCheck::visit(ast::SStatement statement) 
{
}

void TypeCheck::visit(ast::SAssignmentStatement asgnStatement) {
  auto type = asgnStatement->getType(); 

  std::string id = asgnStatement->getTail()->getId();

  // add type to symbol table if new assignment 
  if (type) {
    symbols->addVariableSymbol(id, type);
  } 
  else {
    if (!symbols->getVariableType(id)) {
      std::string err = id + " was used before assignment; line ";
      throw TypeError(err.c_str(), asgnStatement->getLine());
    }
  }

  asgnStatement->getTail()->accept(this);

}

void TypeCheck::visit(ast::SAssignmentStatementTail asgnTail) {
  auto type = symbols->getVariableType(asgnTail->getId());

  auto id = asgnTail->getId();

  asgnTail->getExp()->accept(this);
  
  if (!type->equal(lastType)) {
    std::string err = id + " of type " + std::to_string(type->getType()) + " set to wrong type";
    throw TypeError(err.c_str(), asgnTail->getLine());
  }
}

void TypeCheck::visit(ast::SFunctionCallStatement funcCall) {
  funcCall->getFunctionCall()->accept(this);
}

void TypeCheck::visit(ast::SWhileStatement whileStmt) {
  whileStmt->getExp()->accept(this);
  
  symbols->pushStack();

  whileStmt->getStatements()->accept(this);

  symbols->popStack();
}

void TypeCheck::visit(ast::SIfStatement ifStmt) {
  ifStmt->getExp()->accept(this);
  
  symbols->pushStack();
  
  auto statementList = ifStmt->getStatements();

  if (statementList) {
    statementList->accept(this);
  }

  symbols->popStack();

  auto elseStmt = ifStmt->getElseStmt();

  if (elseStmt) {
    elseStmt->accept(this);
  }
}

void TypeCheck::visit(ast::SElseStatement elseStmt) {
  symbols->pushStack();
  
  auto stmtList = elseStmt->getStatements();

  if (stmtList) {
    stmtList->accept(this);
  }

  symbols->popStack();
}

void TypeCheck::visit(ast::SReturnStatement returnStmt) {
  returnStmt->getExp()->accept(this);
  lastReturnType = lastType;
}

void TypeCheck::visit(ast::SType type) 
{  
}

void TypeCheck::visit(ast::SParameterList params) {
  for (auto param : params->getParameters()) {
    param->accept(this);
  }
}

void TypeCheck::visit(ast::SParameter param) {
  symbols->addVariableSymbol(param->getId(), param->getType());
}

void TypeCheck::visit(ast::SExpression exp) 
{
}

void TypeCheck::visit(ast::SIntExpression intExp) 
{
}

void TypeCheck::visit(ast::SNotExpression notExp) {
  notExp->getExp()->accept(this);
}

void TypeCheck::visit(ast::SCharLiteral charLit) {
  lastType = std::make_shared<ast::Type>(ast::Type(CHAR, false));
}

void TypeCheck::visit(ast::SStringLiteral stringLit) {
  lastType = std::make_shared<ast::Type>(ast::Type(CHAR, true));
}

void TypeCheck::visit(ast::SIntLiteral intLit) {
  lastType = std::make_shared<ast::Type>(ast::Type(INT, false));
}

void TypeCheck::visit(ast::SIntId id) {
  lastType = symbols->getVariableType(id->getId());
  
  if (!lastType) {
    std::string err = id->getId() + " was used before assignment";
    throw TypeError(err.c_str(), id->getLine());
  }
}

void TypeCheck::visit(ast::SIntFunctionCall functionCall) {
  std::string id = functionCall->getId();
  
  auto functionType = symbols->getFunctionReturnType(id);

  if (!functionType) {
    std::string err = "Function " + id + " was called before it was defined";
    throw TypeError(err.c_str(), functionCall->getLine());
  }
    
  auto paramTypes = symbols->getFunctionParamType(id);
  
  if (functionCall->getArgs()) {
    auto arguments = functionCall->getArgs()->getArguments();
    
    if (paramTypes.size() != arguments.size()) {
      std::string err = "Function call to " + id + " was called with invalid arguments";
      throw TypeError(err.c_str(), functionCall->getLine());
    }

    for (unsigned int i = 0; i < arguments.size(); i++) {
      arguments[i]->accept(this);

      if (!paramTypes[i]->equal(lastType)) {
        std::string err = "Function call to " + id + " was called with invalid arguments";
        throw TypeError(err.c_str(), functionCall->getLine());
      }
    }
  } 
  else if (paramTypes.size() != 0) {
    std::string err = "Function call to " + id + " was called with invalid arguments";
    throw TypeError(err.c_str(), functionCall->getLine());
  }

  lastType = functionType;
}

void TypeCheck::visit(ast::SComplexExpression complexExpr) {
  complexExpr->getLHS()->accept(this);

  auto lhsType = lastType;

  complexExpr->getRHS()->accept(this);
  
  if (!lastType->equal(lhsType)) {
    std::string err = "Type error in expression";
    throw TypeError(err, complexExpr->getLine());
  }
}

void TypeCheck::visit(ast::SArguments args) 
{
}
