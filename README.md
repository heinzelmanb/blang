# blang

## Description
A simple C like language

## To build:
    - `mkdir -p bin`
    - `make`

## To run a program
    - `bin/program your_file.blang`

## To run tests
    - `make`
    - `cd test`
    - `make`
    - `./unittest`
    
## Grammar

<program> := <functionList> | e

<functionList> := <function>+

<function> := <type> <id> (<argumentList>) { <statementList> }

<type> := int | char | void | int* | char* | void*;

<statementList> := <statement>*

<statement> := <functionCall> | <ifStatement> | <whileStatement> | <returnStatement>