#pragma once

#include <memory>

#include "lexer.h"
#include "token.h"
#include "ast.h"

class Parser;

typedef std::shared_ptr<Parser> SParser;



class Parser {
  public: 
	Parser(SLexer lexer); 

	/**
	 * Description: Parses the program
	 * TODO generate AST
	 */
	ast::SProgram parse(std::ostream& outputStream);
  private:
	// private member vars
	SLexer lexer;
	SToken currentToken;
	SToken peekToken;

	// helper funcs
	
	/**
	 * Description: Gets the next token in the stream
	 * Sets currentToken, and returns it
	 */
	SToken nextToken();

	/**
	 * Description: Checks next token in the stream.
	 * Returns it.
	 */
	SToken peek();

	/**
	 * Description: Checks if t1 == t2
	 * Throws: Error if t1 != t2
	 */ 
	void assert(TOKEN_TYPE t1, TOKEN_TYPE t2) const;

	/**
	 * Description: Gets the line of the current token
	 */
	unsigned int getLine() const;	
	
	// parsing functions
	ast::SFunctionList functionList();
	ast::SFunctionList functionListTail();
	ast::SFunction function();
	ast::SIntFunctionCall functionCall(std::string id);

	ast::SParameterList paramList();
	ast::SParameterList paramListTail();
	ast::SParameter param();

	ast::SStatementList statementList();
	ast::SStatementList statementListTail();
	ast::SStatement statement();

	ast::SAssignmentStatement assignmentStatement();
	ast::SAssignmentStatementTail assignmentStatementTail(std::string id);

	ast::SIfStatement ifStatement();
	ast::SElseStatement ifStatementTail();
	ast::SWhileStatement whileStatement();
	ast::SReturnStatement returnStatement();

	ast::SExpression expression();
	ast::SIntExpression intExpression();
	ast::SComplexExpression intExpressionTail();

	ast::SIntExpression intValue();
	ast::SArguments argumentList();
	ast::SArguments argumentListTail();

	ast::SType type();
	bool peekType();	

	bool peekOp();	
	TOKEN_TYPE op();	
};
