#include "lexer.h"
#include <iostream> 
#include <cctype> 
#include <exception>

// default constructor
Lexer::Lexer(std::ifstream* program) {
  this->program = program;
  lineNumber = 1;
}

/**
 * are there more tokens
 */
bool Lexer::hasMore() {
  return !(peekExplicit() == EOF);
}

/**
 * Return the next char
 */
char Lexer::nextChar() {
  char c;  
  do {
    program->get(c);
    if (c == '\n') {
      lineNumber++;
    }
  } while (isspace(c) && hasMore());
  return c;
}

/**
 * gets the next token in the stream
 */
char Lexer::nextCharExplicit() {
  char c;
  program->get(c);
  return c;
}

/**
 * check the next character
 */
char Lexer::peek() {
  char c = program->peek();

  while (isspace(program->peek())) {
    if (c == '\n') {
      lineNumber++;
    }
    program->get(c);
  }
  return c;
}

/**
 * Peeks the next character, will NOT skip whitespace
 */
char Lexer::peekExplicit() {
  return program->peek();
}

/**
 * Get the next token in the stream
 */
SToken Lexer::nextToken() {
  std::string raw_token = "";

  char c = nextChar();
  raw_token += c;

  if (!hasMore()) {
    return std::make_shared<Token>(Token(END, lineNumber));
  }
  
  //keyword, id
  if (isalpha(c)) {
    while ((isalpha(peekExplicit()) || isdigit(peekExplicit()) || peekExplicit() == '_') && hasMore()) {
      raw_token += nextCharExplicit();
    }

    // return the token
    return getTextToken(raw_token);
  }
  // integer
  else if (isdigit(c)) {
    while (isdigit(peekExplicit())) {
      raw_token += nextCharExplicit();
    }
    return std::make_shared<Token>(INT_VAL, lineNumber, raw_token);
  } 
  // char literal
  else if (c == '\'') {
    raw_token = nextChar();
    
    if (raw_token[0] == '\\') {
      char escapeCode = nextCharExplicit();
      switch (escapeCode) {
        case 'a':
          raw_token = "\a";
          break;
        case 'b':
          raw_token = "\b";
          break;
        case 'f':
          raw_token = "\f";
          break;
        case 'n':
          raw_token = "\n";
          break;
        case 'r':
          raw_token = "\r";
          break;
        case 't':
          raw_token = "\t";
          break;
        case 'v':
          raw_token = "\v";
          break;
        case '\\':
          raw_token = "\\";
          break;
        default:
          return std::make_shared<Token>(Token(ERROR, lineNumber, raw_token));
      }
    }

    // skip the next ' 
    nextCharExplicit();
    return std::make_shared<Token>(CHAR_VAL, lineNumber, raw_token);
  }
  // String Literal
  else if (c == '"') {
    raw_token = "";
    while (peekExplicit() != '"') {
      raw_token += nextCharExplicit();
    }
    nextChar();
    return std::make_shared<Token>(STRING_VAL, lineNumber, raw_token);
  }
  // Comments
  else if (c == '/') {

    // single line
    if (peekExplicit() == '/') { 
      nextCharExplicit();
      while (c != '\n') {
        c = nextCharExplicit();
      }
      return nextToken();
    }
    // multi-line
    else if (peekExplicit() == '*') {
      nextCharExplicit();

      char prev = nextCharExplicit();
      char cur = nextCharExplicit();

      while (!(prev == '*' && cur == '/')) {
        prev = cur;
        cur = nextCharExplicit();
      }
      return nextToken();
    }
  }
  //op or syntax piece
  return getOtherToken(raw_token);
}

/**
 * Description: Outputs Tokens to the given output stream
 */
void Lexer::outputTokenStream(std::ostream& out) {
  SToken tok;
  do {
    tok = nextToken();

    if (tok == nullptr) {
      continue;
    }
    else {
      out << tok->getType() << " " << tok->getLexeme() << std::endl;
    }
  } while (tok->getType() != END); 
}


/**
 * If it is a text based token, get it
 */
SToken Lexer::getTextToken(std::string in) {
  auto isIn = Token::keywords.find(in);
  if (isIn != Token::keywords.end()) {
    return std::make_shared<Token>(Token(isIn->second, lineNumber, in));
  }
  return std::make_shared<Token>(Token(ID, lineNumber, in));
}

/**
 * Given that the next token is not an ID, or number
 * return the token
 */
SToken Lexer::getOtherToken(std::string in) {

  // check if multi-character token operator
  if (in[0] == '=' && peekExplicit() == '=') {
    nextCharExplicit();
    return std::make_shared<Token>(Token(EQUAL_EQUAL, lineNumber, in));
  }
  else if (in[0] == '!' && peekExplicit() == '=') {
    nextCharExplicit();
    return std::make_shared<Token>(Token(NOT_EQUAL, lineNumber, in));
  }
  do {
    auto isIn = Token::keywords.find(in); 
    if (isIn != Token::keywords.end()) {
      return std::make_shared<Token>(Token(isIn->second, lineNumber, in));
    }
    in += nextChar();
  } while (!(isalpha(peek()) || isdigit(peek())));
  return std::make_shared<Token>(Token(ERROR, lineNumber));
}
