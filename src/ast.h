#pragma once

#include <memory>
#include <ostream>
#include <vector>

#include "token.h"

class Visitor;

namespace ast {

// forward declarations
class ASTNode;
class Program; 
class FunctionList;
class Function;
class ExternFunction;
class FunctionImpl;
class StatementList;
class Statement;
class AssignmentStatement;
class AssignmentStatementTail;
class WhileStatement;
class FunctionCallStatement;
class IfStatement;
class ElseStatement;
class ReturnStatement;
class Type;
class ParameterList; 
class Parameter; 
class Expression;
class NotExpression;
class CharLiteral;
class IntExpression;
class IntLiteral;
class StringLiteral;
class IntId;
class IntFunctionCall;
class ComplexExpression;
class Arguments;

// smart pointer typedefs
typedef std::shared_ptr<ASTNode> SASTNode;
typedef std::shared_ptr<Program> SProgram;
typedef std::shared_ptr<FunctionList> SFunctionList;
typedef std::shared_ptr<Function> SFunction;
typedef std::shared_ptr<FunctionImpl> SFunctionImpl;
typedef std::shared_ptr<ExternFunction> SExternFunction;
typedef std::shared_ptr<StatementList> SStatementList;
typedef std::shared_ptr<Statement> SStatement;
typedef std::shared_ptr<AssignmentStatement> SAssignmentStatement;
typedef std::shared_ptr<AssignmentStatementTail> SAssignmentStatementTail;
typedef std::shared_ptr<WhileStatement> SWhileStatement;
typedef std::shared_ptr<FunctionCallStatement> SFunctionCallStatement;
typedef std::shared_ptr<IfStatement> SIfStatement;
typedef std::shared_ptr<ElseStatement> SElseStatement;
typedef std::shared_ptr<ReturnStatement> SReturnStatement;
typedef std::shared_ptr<Type> SType;
typedef std::shared_ptr<ParameterList> SParameterList;
typedef std::shared_ptr<Parameter> SParameter;
typedef std::shared_ptr<Expression> SExpression;
typedef std::shared_ptr<IntExpression> SIntExpression;
typedef std::shared_ptr<NotExpression> SNotExpression;
typedef std::shared_ptr<CharLiteral> SCharLiteral;
typedef std::shared_ptr<StringLiteral> SStringLiteral;
typedef std::shared_ptr<IntLiteral> SIntLiteral;
typedef std::shared_ptr<IntId> SIntId;
typedef std::shared_ptr<IntFunctionCall> SIntFunctionCall;
typedef std::shared_ptr<ComplexExpression> SComplexExpression;
typedef std::shared_ptr<Arguments> SArguments;

/**
 * AST Node Abstract Base class
 */
class ASTNode {
  public:
	ASTNode(unsigned int line);

	virtual void print(std::ostream& output, int tabs=0)=0;

	virtual void accept(Visitor* acceptor)=0;

	virtual unsigned int getLine() final;
  protected:	
	void printTabs(std::ostream& output, int tabs);
  private:	
	unsigned int line;
};

/**
 * Program Node. The root of the ast
 */
class Program : public ASTNode {
  public:
	Program(SFunctionList list);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SFunctionList getFunctionList() const;
  private:
	SFunctionList functions;
};

/**
 * type node
 */
class Type : public ASTNode {
  public:
	Type(TOKEN_TYPE type, bool hasPointer, unsigned int line);

	Type(TOKEN_TYPE type, bool hasPointer);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	bool equal(SType other);

	TOKEN_TYPE getType() const;

	bool isPointer() const;
  private:
	TOKEN_TYPE type;	
	bool hasPointer;
}; 

/**
 * Function List Node
 */
class FunctionList : public ASTNode {
  public:
	FunctionList(unsigned int line) : ASTNode(line) {};

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	void addFunction(SFunction func);

	std::vector<SFunction> getFunctions() const;
  private:
	std::vector<SFunction> functions;
};

/**
 * Function Node
 */
class Function : public ASTNode {
  public:
	Function(SType type, SParameterList parameters, std::string id, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0)=0;
	
	virtual void accept(Visitor* acceptor)=0;

	SType getType() const;

	SParameterList getParameters() const;

	std::string getId() const;
  private:
	SType type;
	SParameterList parameters;
	std::string id;
};

/**
 * Extern Function Node
 */
class ExternFunction : public Function {
  public:
	ExternFunction(SType type, SParameterList parameters, std::string id, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);
};

/**
 * Function Implementation Node
 */
class FunctionImpl : public Function {
  public:
	FunctionImpl(SType type, SParameterList parameters, std::string id, SStatementList list, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SStatementList getStmtList() const;
  private:
	SStatementList stmtList;
};

/**
 * Abstract base class Expression
 */
class Expression : public ASTNode {
  public:
	Expression(unsigned int line) : ASTNode(line) {};

	virtual void print(std::ostream& output, int tabs=0)=0;
	
	virtual void accept(Visitor* acceptor)=0;
};

/**
 * Char Literal
 */
class CharLiteral : public Expression {
  public:
	CharLiteral(char literal, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);
  private:
	char literal;
};

/**
 * String Literal
 */
class StringLiteral : public Expression {
  public:
	StringLiteral(std::string literal, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);
  private:
	std::string literal;
};

/**
 * Int Expression
 */
class IntExpression : public Expression {
  public:
	IntExpression(unsigned int line) : Expression(line) {};

	virtual void print(std::ostream& output, int tabs=0)=0;
	
	virtual void accept(Visitor* acceptor)=0;
};

/**
 * Int Literal
 */
class IntLiteral : public IntExpression {
  public:
	IntLiteral(int literal, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);
  private:
	int literal;
};

/**
 * Id
 */
class IntId : public IntExpression {
  public:
	IntId(std::string id, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	std::string getId() const;
  private:
	std::string id;
};

/**
 * Argument Node (parameters to a function call)
 */
class Arguments : public ASTNode {
  public:
	Arguments(unsigned int line) : ASTNode(line) {};

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	std::vector<SExpression> getArguments() const;

	void addArgument(SExpression arg);
  private:
	std::vector<SExpression> args;
};

/**
 * Function Call
 */
class IntFunctionCall : public IntExpression {
  public:
	IntFunctionCall(std::string functionId, SArguments args, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	std::string getId() const;

	SArguments getArgs() const;
  private:
	std::string functionId;
	SArguments args;
};

/**
 * Complex Expression
 */
class ComplexExpression : public IntExpression {
  public:
	ComplexExpression(TOKEN_TYPE op, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	void setLHS(SExpression exp);

	void setRHS(SExpression exp);

	SExpression getLHS() const;
	
	SExpression getRHS() const;
  private:
	TOKEN_TYPE op;
	SExpression lhs;
	SExpression rhs;
};

/**
 * Not Expression
 */
class NotExpression : public IntExpression {
  public:
	NotExpression(SIntExpression exp, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SIntExpression getExp() const;
  private:
	SIntExpression exp;
};

/**
 * Statement List Node
 */
class StatementList : public ASTNode {
  public:
	StatementList(unsigned int line) : ASTNode(line) {};

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	void addStatement(SStatement statement);

	std::vector<SStatement> getStatements() const;
  private:
	std::vector<SStatement> statements;
};

/**
 * Statement Node
 */
class Statement : public ASTNode {
  public:
	Statement(unsigned int line) : ASTNode(line) {}

	virtual void print(std::ostream& output, int tabs=0)=0;
	
	virtual void accept(Visitor* acceptor)=0;
};

/**
 * Assignment Statement Node
 */
class AssignmentStatement : public Statement {
  public:
	AssignmentStatement(SType type, SAssignmentStatementTail tail, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SType getType() const;

	SAssignmentStatementTail getTail() const;
  private:
	SType type;
	SAssignmentStatementTail tail;
};

/**
 * Function Call Statement
 */
class FunctionCallStatement : public Statement {
  public: 
	FunctionCallStatement(SIntFunctionCall functionCall, unsigned int line);

	SIntFunctionCall getFunctionCall() const;
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);
  private:
	SIntFunctionCall functionCall;
};

/**
 * Assignment Statement Node
 */
class AssignmentStatementTail : public ASTNode {
  public:
	AssignmentStatementTail(std::string id, SExpression exp, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	std::string getId() const;

	SExpression getExp() const;
  private:
	std::string id;
	SExpression exp;
};

/**
 * While statement
 */
class WhileStatement : public Statement {
  public:
	WhileStatement(SStatementList statements, SIntExpression exp, unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SIntExpression getExp() const;

	SStatementList getStatements() const;
  private:
	SStatementList statements;
	SIntExpression exp;
};

/**
 * Else statement
 */
class ElseStatement : public Statement {
  public:
	ElseStatement(SStatementList statements, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SStatementList getStatements() const;
  private:
	SStatementList statements;
};

/**
 * If statement
 */
class IfStatement : public Statement {
  public:
	IfStatement(SIntExpression exp, SStatementList statements, SElseStatement elseStmt, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SIntExpression getExp() const;

	SStatementList getStatements() const;

	SElseStatement getElseStmt() const;
  private:
	SIntExpression exp;
	SStatementList statements;
	SElseStatement elseStmt;
};

/**
 * Return statement
 */
class ReturnStatement : public Statement {
  public:
	ReturnStatement(SExpression exp, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SExpression getExp() const;
  private:
	SExpression exp;
};

/**
 * ParameterList Node
 */
class ParameterList : public ASTNode {
  public:
	ParameterList(unsigned int line);

	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	void addParameter(SParameter param);

	std::vector<SParameter> getParameters() const;
  private:
	std::vector<SParameter> parameters;
};

/**
 * Parameter Node
 */
class Parameter : public ASTNode {
  public:
	Parameter(SType type, std::string id, unsigned int line);
	
	virtual void print(std::ostream& output, int tabs=0);
	
	virtual void accept(Visitor* acceptor);

	SType getType() const;

	std::string getId() const;
  private:
	SType type;
	std::string id;
};

}
