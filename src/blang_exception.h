#pragma once

/**
 * This is where exceptions go.
 */

#include <string>
 
class BlangException : public std::exception {
  public:
	BlangException() {};

	BlangException(const char* msg) : err_msg(msg) {};
			
	~BlangException() throw() {};
			
	virtual const char *what() const throw() { return this->err_msg.c_str(); };
  protected:
	std::string err_msg;
};

/**
 * Please throw this exception when a TYPE ERROR occurs
 */
class TypeError : public BlangException {
  public:
	/**
	 * Description: 
	 * Msg -> Message to show, 
	 * line -> the line the error occured ont
	 */
	TypeError(std::string msg, unsigned int line) {
	  err_msg = "Type Error: " + msg + "; on line " + std::to_string(line);
	}

	~TypeError() throw() {};	
};

/**
 * Please throw this exception when a PARSE ERROR occurs
 */
class ParseError : public BlangException {
  public:
	/**
	 * Description: 
	 * Msg -> Message to show, 
	 * line -> the line the error occured ont
	 */
	ParseError(std::string msg, unsigned int line) {
	  err_msg = "Parse Error: " + msg + "; on line " + std::to_string(line);
	}

	~ParseError() throw() {};	
};
