#include <iostream>
#include <exception> 
#include "parser.h"
#include "blang_exception.h"

using namespace ast;

// constructor
Parser::Parser(SLexer lexer): lexer(lexer) {
  currentToken = nullptr;
  peekToken = nullptr;
}

/** 
 * Parses the program,  
 */
SProgram Parser::parse(std::ostream& outputStream) {
  try {
    SFunctionList functions = functionList();
    assert(END, nextToken()->getType());  
    return std::make_shared<Program>(Program(functions));
  }
  catch (ParseError e) {
    outputStream << e.what() << std::endl;
    return nullptr;
  }
}

/**
 * Return: the next token in the stream */
SToken Parser::nextToken() {
  if (peekToken != nullptr) {
    currentToken = peekToken;
    peekToken = nullptr;
  }
  else {
    currentToken = lexer->nextToken();
  }
  return currentToken; 
}

/**
 * Return: Checks the next token in the stream without
 * truly fetching it
 */
SToken Parser::peek() {
  if (peekToken == nullptr) {
    peekToken = lexer->nextToken();
    return peekToken;
  }
  return peekToken;
}

/**
 * Description: Asserts t1 == t2
 * Throws: std::exception if t1 != t2
 */
void Parser::assert(TOKEN_TYPE t1, TOKEN_TYPE t2) const {
  if (t1 != t2) {
    std::string err = "Expected " + Token::getNameFromType(t1) + " got " + Token::getNameFromType(t2);
    throw ParseError(err, getLine());
  }
}

/**
 * Description: Gets line of cur token
 */
unsigned int Parser::getLine() const {
  if (currentToken) {
    return currentToken->getLine();
  }
  return 0;
}

/**
 * Description: Parses a function list
 */
SFunctionList Parser::functionList() {
  FunctionList list = FunctionList(getLine());
  list.addFunction(function());

  SFunctionList tail = functionListTail();

  if (tail != nullptr) {
    for (auto func : tail->getFunctions()) {
      list.addFunction(func);
    }
  }

  return std::make_shared<FunctionList>(list);
}

/**
 * Description: Parses function list tail
 */
SFunctionList Parser::functionListTail() {
  if (peekType()) {
    SFunctionList list = functionList();
    return list ; 
  }
  return nullptr;
}

/**
 * Description: Parses a function 
 */
SFunction Parser::function() { 
  if (peekType()) {
    std::string id;

    SType returnType = type();
    // check id
    assert(ID, nextToken()->getType());

    id = currentToken->getLexeme();

    assert(LPAREN, nextToken()->getType());

    SParameterList plist = paramList(); 

    assert(RPAREN, nextToken()->getType());

    assert(LBRACE, nextToken()->getType());
    
    SStatementList stmtList = statementList();

    assert(RBRACE, nextToken()->getType());

    return std::make_shared<FunctionImpl>(FunctionImpl(returnType, plist, id, stmtList, getLine()));
  }
  else if (peek()->getType() == EXTERN) {
    assert(EXTERN, nextToken()->getType());

    SType returnType = type();
    
    assert(ID, nextToken()->getType());
    
    std::string id = currentToken->getLexeme();

    assert(LPAREN, nextToken()->getType());

    SParameterList plist = paramList(); 

    assert(RPAREN, nextToken()->getType());

    assert(SEMI, nextToken()->getType());
    
    return std::make_shared<ExternFunction>(ExternFunction(returnType, plist, id, getLine()));
  }
  throw ParseError("Expected a function", currentToken->getLine());
}

/**
 * Description: Parses a parameter list 
 */
SParameterList Parser::paramList() {
  if (peekType()) {
    ParameterList plist = ParameterList(getLine());

    SParameter functionParameter = param();
    
    plist.addParameter(functionParameter);

    SParameterList tail = paramListTail();

    if (tail != nullptr) {
      for (auto param : tail->getParameters()) {
        plist.addParameter(param);
      }
    }
    return std::make_shared<ParameterList>(plist);
  }
  return nullptr;
}

/**
 * Description: Parses a parameter list tail
 */
SParameterList Parser::paramListTail() {
  if (peek()->getType() == COMMA) {
    nextToken();
    return paramList();
  }
  return nullptr;
}

/**
 * Description: Parses a parameter 
 */
SParameter Parser::param() {
  SType parameterType = type();

  assert(ID, nextToken()->getType());

  std::string id = currentToken->getLexeme();

  return std::make_shared<Parameter>(Parameter(parameterType, id, getLine()));
}

/**
 * Description: Parses a statement list 
 */
SStatementList Parser::statementList() {
  StatementList slist = StatementList(getLine()); 
  if (peek()->getType() != RBRACE) {
    slist.addStatement(statement());

    auto tail = statementListTail();
    if (tail != nullptr) {
      for (auto tailStmnt: tail->getStatements()) {
        slist.addStatement(tailStmnt);
      }
    }
  }
  return std::make_shared<StatementList>(slist);
}

/**
 * Description: Parses a statement list tail 
 */
SStatementList Parser::statementListTail() {
  return statementList();
}

/**
 * Description: Parses a statement 
 */
SStatement Parser::statement() {
  if (peekType()) {
    SAssignmentStatement as = assignmentStatement();
    assert(SEMI, nextToken()->getType());
    return as;
  }  
  else if (peek()->getType() == ID) {
    nextToken();
    auto id = currentToken->getLexeme();
 
    if (peek()->getType() == LPAREN) {
      nextToken();
      //TODO Function call
      auto funcCall = functionCall(id);

      assert(RPAREN, nextToken()->getType());
      assert(SEMI, nextToken()->getType());
      return std::make_shared<FunctionCallStatement>(FunctionCallStatement(funcCall, getLine()));
    }
    else {
      SAssignmentStatementTail tail = assignmentStatementTail(id);
      assert(SEMI, nextToken()->getType());
      return std::make_shared<AssignmentStatement>(AssignmentStatement(nullptr, tail, getLine()));
    }
  }
  else if (peek()->getType() == IF) {
    return ifStatement();
  }
  else if (peek()->getType() == WHILE) {
    return whileStatement();
  }
  else if (peek()->getType() == RETURN) {
    auto retStmt = returnStatement();
    assert(SEMI, nextToken()->getType());
    return retStmt;
  }
  throw ParseError(std::string("Expected a statement"), getLine());
}

/**
 * Description: Parses an assignment statement 
 */
SAssignmentStatement Parser::assignmentStatement() {
  SType assignmentType = nullptr;
  if (peekType()) {
    assignmentType = type();
  }
  std::string id;

  assert(ID, nextToken()->getType());
  
  id = currentToken->getLexeme();

  SAssignmentStatementTail tail = assignmentStatementTail(id);
  return std::make_shared<AssignmentStatement>(AssignmentStatement(assignmentType, tail, getLine()));
}

/**
 * Description: Parses an assignment statement 
 */
SAssignmentStatementTail Parser::assignmentStatementTail(std::string id) { 
  // array accessor
  if (peek()->getType() == LBRACK) {
    nextToken();
    intExpression();
    assert(nextToken()->getType(), RBRACK);
  }

  assert(EQUAL, nextToken()->getType());
  auto exp = expression(); 
  return std::make_shared<AssignmentStatementTail>(AssignmentStatementTail(id, exp, getLine()));
}

/**
 * Description: Parses an if statement
 */
SIfStatement Parser::ifStatement() {
  assert(nextToken()->getType(), IF);
  assert(nextToken()->getType(), LPAREN);

  SIntExpression exp = intExpression();

  assert(nextToken()->getType(), RPAREN);
  
  SStatementList slist;

  if (peek()->getType() == LBRACE) {
    nextToken();
    slist = statementList();
    assert(nextToken()->getType(), RBRACE);
  } 
  else {
    auto stmt = statement();

    StatementList notSafeList = StatementList(getLine());
    notSafeList.addStatement(stmt);
    slist = std::make_shared<StatementList>(notSafeList);
  }

  SElseStatement elseStmt = ifStatementTail();

  return std::make_shared<IfStatement>(IfStatement(exp, slist, elseStmt, getLine()));
}

/**
 * Description: Parses an if statement
 */
SElseStatement Parser::ifStatementTail() {
  if (peek()->getType() == ELSE) {
    nextToken();
    
    SStatementList slist;

    if (peek()->getType() == LBRACE) { 
      nextToken();
      slist = statementList();
      assert(nextToken()->getType(), RBRACE);
    } 
    else {
      auto stmt = statement();
      StatementList notSafeList = StatementList(getLine());
      notSafeList.addStatement(stmt);
      slist = std::make_shared<StatementList>(notSafeList);
    }
    return std::make_shared<ElseStatement>(ElseStatement(slist, getLine()));
  }
  return nullptr;
}

/**
 * Description: Parses a while statement
 */
SWhileStatement Parser::whileStatement() {
  assert(nextToken()->getType(), WHILE);
  assert(nextToken()->getType(), LPAREN);

  SIntExpression exp = intExpression();

  assert(nextToken()->getType(), RPAREN);

  if (peek()->getType() == LBRACE) {
    nextToken();
    SStatementList stmts = statementList();
    assert(nextToken()->getType(), RBRACE);

    return std::make_shared<WhileStatement>(WhileStatement(stmts, exp, getLine()));
  } 
  else {
    StatementList stmts = StatementList(getLine());
    stmts.addStatement(statement());

    SStatementList safeStmts = std::make_shared<StatementList>(stmts);

    return std::make_shared<WhileStatement>(WhileStatement(safeStmts, exp, getLine()));
  }
}

/**
 * Description: Parses a return statement
 */
SReturnStatement Parser::returnStatement() { 
  assert(nextToken()->getType(), RETURN);
  SExpression exp = expression();
  return std::make_shared<ReturnStatement>(ReturnStatement(exp, getLine()));;
}

/**
 * Description: Parses an integer expression,
 * TODO: Should add string literals */
SExpression Parser::expression() {
  if (peek()->getType() == LPAREN) {
    return intExpression();
  } 
  else if (peek()->getType() == CHAR_VAL) {
    nextToken();
    return std::make_shared<CharLiteral>(CharLiteral(currentToken->getLexeme()[0], getLine()));
  }
  else if (peek()->getType() == STRING_VAL) { 
    nextToken();

    return std::make_shared<StringLiteral>(StringLiteral(currentToken->getLexeme(), getLine()));
  }
  else {
    return intExpression();
  }

  return nullptr;
}

/**
 * Description: Parses an int expression
 */
SIntExpression Parser::intExpression() {
  SIntExpression iExpr;

  if (peek()->getType() == LPAREN) {
    nextToken();
    iExpr = intExpression();
    assert(nextToken()->getType(), RPAREN);
  }
  else if (peek()->getType() == NOT) {
    nextToken();
    SIntExpression notExp = intExpression(); 
    iExpr = std::make_shared<NotExpression>(NotExpression(notExp, getLine()));
  }
  else {
    iExpr = intValue();
  }
  SComplexExpression comExp = intExpressionTail();

  if (comExp) {
    comExp->setLHS(iExpr);
    return comExp;
  }
  return iExpr;
}

/**
 * Description: Parses an int expression tail
 */
SComplexExpression Parser::intExpressionTail() {
  if (peekOp()) {
    TOKEN_TYPE expOp = op();
    SExpression rhs = intExpression();

    ComplexExpression complexExpr = ComplexExpression(expOp, getLine());
    complexExpr.setRHS(rhs);
    return std::make_shared<ComplexExpression>(complexExpr);
  }
  return nullptr;
}

/**
 * Description: Looks for an int val
 */
SIntExpression Parser::intValue() {
  if (peek()->getType() == INT_VAL) {
    nextToken();
    return std::make_shared<IntLiteral>(IntLiteral(atoi(currentToken->getLexeme().c_str()), getLine()));
  }
  else if (peek()->getType() == ID) {
    nextToken();
    
    std::string id = currentToken->getLexeme();

    // function call
    if (peek()->getType() == LPAREN) {
      nextToken();
      auto funcCall = functionCall(id);
      assert(RPAREN, nextToken()->getType());
      return funcCall;
    }
    return std::make_shared<IntId>(IntId(id, getLine()));
  } 
  throw ParseError(std::string("Expected Int Value"), getLine());
}

SIntFunctionCall Parser::functionCall(std::string id) {
  SArguments args = argumentList();
  return std::make_shared<IntFunctionCall>(IntFunctionCall(id, args, getLine()));
}

/**
 * Description: Parses an arugument list. I.E. the
 * parameters when calling a function
 */
SArguments Parser::argumentList() {
  if (peek()->getType() != RPAREN) {
    Arguments args = Arguments(getLine());
    args.addArgument(expression());

    auto tail = argumentListTail();
    if (tail) {
      for (auto arg : tail->getArguments()) {
        args.addArgument(arg);
      }
    }
    return std::make_shared<Arguments>(args);
  }
  return nullptr;
}

/**
 * Description: Parses an arugment list tail
 */
SArguments Parser::argumentListTail() {
  if (peek()->getType() == COMMA) {
    nextToken();
    return argumentList();
  }
  return nullptr;
}

/**
 * Description: Checks if next tokens are a type
 * Throws: if they are not a type
 */
SType Parser::type() {
  if (peekType()) {
    nextToken();

    TOKEN_TYPE t = currentToken->getType();
    bool hasPointer = false;

    if (peek()->getType() == STAR) {
      nextToken();
      hasPointer = true;
    }
    return std::make_shared<Type>(Type(t, hasPointer, getLine()));
  } 
  else {
    throw ParseError(std::string("Expected a type"), getLine());
  }
  return nullptr;
}

/**
 * Description: PEEKS, to see if next tokens are a type
 * Return: true if next tokens are a type 
 */
bool Parser::peekType() {
  auto tp = peek()->getType();
  return (tp == INT || tp == CHAR || tp == VOID);
}

/**
 * Description: PEEKS, to see if next tokens are an op 
 * Return: true if next tokens are an op 
 */
bool Parser::peekOp() {
  auto tp = peek()->getType();
  return (
      tp == ADD || 
      tp == SUBTRACT || 
      tp == STAR || 
      tp == DIVIDE ||
      tp == GREATER_THAN ||
      tp == LESS_THAN ||
      tp == AND ||
      tp == OR ||
      tp == EQUAL ||
      tp == NOT ||
      tp == NOT_EQUAL ||
      tp == EQUAL_EQUAL 
  );
}

/**
 * Description: asserts that an op comes next 
 */
TOKEN_TYPE Parser::op() {
  auto tp = nextToken()->getType();
  if (
      tp == ADD || 
      tp == SUBTRACT || 
      tp == STAR || 
      tp == DIVIDE ||
      tp == GREATER_THAN ||
      tp == LESS_THAN ||
      tp == AND ||
      tp == OR  || 
      tp == EQUAL_EQUAL || 
      tp == NOT_EQUAL || 
      tp == NOT
  ) {
    return tp;
  }
  throw ParseError(std::string("Expected an operator"), getLine());
}
