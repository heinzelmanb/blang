#pragma once

#include <memory>

#include "ast.h"

class Visitor;

typedef std::shared_ptr<Visitor> SVisitor;

class Visitor {
  public:
	virtual void visit(ast::SProgram program)=0;
	virtual void visit(ast::SFunctionList functionList)=0;
	virtual void visit(ast::SFunction function)=0;
	virtual void visit(ast::SExternFunction function)=0;
	virtual void visit(ast::SFunctionImpl function)=0;
	virtual void visit(ast::SStatementList statementList)=0;
	virtual void visit(ast::SStatement statement)=0;
	virtual void visit(ast::SAssignmentStatement asgnStatement)=0;
	virtual void visit(ast::SAssignmentStatementTail asgnTail)=0;
	virtual void visit(ast::SWhileStatement whileStmt)=0;
	virtual void visit(ast::SIfStatement ifStmt)=0;
	virtual void visit(ast::SElseStatement elseStmt)=0;
	virtual void visit(ast::SReturnStatement returnStmt)=0;
	virtual void visit(ast::SFunctionCallStatement functionCall)=0;
	virtual void visit(ast::SType type)=0;
	virtual void visit(ast::SParameterList params)=0;
	virtual void visit(ast::SParameter param)=0;
	virtual void visit(ast::SExpression exp)=0;
	virtual void visit(ast::SIntExpression intExp)=0;
	virtual void visit(ast::SNotExpression notExp)=0;
	virtual void visit(ast::SCharLiteral charLit)=0;
	virtual void visit(ast::SStringLiteral charLit)=0;
	virtual void visit(ast::SIntLiteral intLit)=0;
	virtual void visit(ast::SIntId id)=0;
	virtual void visit(ast::SIntFunctionCall functionCall)=0;
	virtual void visit(ast::SComplexExpression complexExpr)=0;
	virtual void visit(ast::SArguments args)=0;
};

