#include "ast.h" 
#include "visitor.h"

using namespace ast;

/*=======================
 * AST Node 
 *=======================
 */

ASTNode::ASTNode(unsigned int line) {
  this->line = line;
}

void ASTNode::printTabs(std::ostream& output, int tabs) {
  for (int i = 0; i < tabs; i++)
      output << "\t";
}

unsigned int ASTNode::getLine() {
  return line;
}

/*=======================
 * Program
 *=======================
 */
Program::Program(SFunctionList list) : ASTNode(0) {
  this->functions = list;
}

void Program::print(std::ostream& output, int tabs) {
  printTabs(output, tabs);
  functions->print(output, tabs);
}

void Program::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<Program>(*this));
}

SFunctionList Program::getFunctionList() const {
  return functions;
}

/*=======================
 * Function List 
 *=======================
 */
void FunctionList::print(std::ostream& output, int tabs) {
  printTabs(output, tabs);
  output << "FUNCTION LIST\n";
  for (auto func: functions) {
    func->print(output, tabs+1);
  }
}

void FunctionList::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<FunctionList>(*this));
}

void FunctionList::addFunction(SFunction func) {
  functions.push_back(func);
}

std::vector<SFunction> FunctionList::getFunctions() const {
  return functions;
}

/*=======================
 * Function 
 *=======================
 */
Function::Function(SType type, SParameterList parameters, std::string id, unsigned int line) : ASTNode(line) {
  this->id = id;
  this->type = type;
  this->parameters = parameters;
}

SType Function::getType() const {
  return type;
}

SParameterList Function::getParameters() const {
  return parameters;
}

std::string Function::getId() const {
  return id;
}

/*=======================
 * ExternFunction 
 *=======================
 */
ExternFunction::ExternFunction(SType type, SParameterList parameters, std::string id, unsigned int line) : Function(type, parameters, id, line) 
{
}
	
void ExternFunction::print(std::ostream& output, int tabs) {
  output << "extern ";
  getType()->print(output, tabs);
  output << getId() << "(";
  getParameters()->print(output, tabs);
  output << ");";
}
	
void ExternFunction::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<ExternFunction>(*this));
}

/*=======================
 * Function Implementation
 *=======================
 */
FunctionImpl::FunctionImpl(SType type, SParameterList parameters, std::string id, SStatementList list, unsigned int line) : Function(type, parameters, id, line) {
  this->stmtList = list;
}

void FunctionImpl::print(std::ostream& output, int tabs) {
  getType()->print(output, tabs);
  output << getId() << "(";
  getParameters()->print(output, tabs);
  output << ");";
  getStmtList()->print(output, tabs);
}
	
void FunctionImpl::accept(Visitor* acceptor) {
  acceptor->visit(std::make_shared<FunctionImpl>(*this));
}

SStatementList FunctionImpl::getStmtList() const {
  return stmtList;
}


/*=======================
 * Statement List 
 *=======================
 */
void StatementList::print(std::ostream& output, int tabs) {
  for (auto stmt: statements) {
    if (stmt != nullptr) {
      stmt->print(output, tabs);
      output << std::endl;
    }
  }
}

void StatementList::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<StatementList>(*this));
}

void StatementList::addStatement(SStatement statement) {
  statements.push_back(statement);
}

std::vector<SStatement> StatementList::getStatements() const {
  return statements;
}

/*=======================
 * Assignment Statement 
 *=======================
 */
AssignmentStatement::AssignmentStatement(SType type, SAssignmentStatementTail tail, unsigned int line) : Statement(line){
  this->type = type;
  this->tail = tail;
}


void AssignmentStatement::print(std::ostream& output, int tabs) {
  printTabs(output, tabs); 
  if (type != nullptr) {
    type->print(output);
  }
  tail->print(output);
}

void AssignmentStatement::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<AssignmentStatement>(*this));
}

SType AssignmentStatement::getType() const {
  return type;
}

SAssignmentStatementTail AssignmentStatement::getTail() const {
  return tail;
}


/*=======================
 * Assignment Statement Tail
 *=======================
 */
AssignmentStatementTail::AssignmentStatementTail(std::string id, SExpression exp, unsigned int line) : ASTNode(line){
  this->id = id;
  this->exp = exp;
}

void AssignmentStatementTail::print(std::ostream& output, int tabs) {
  output << id << " = ";
  exp->print(output, tabs);
  output << ";";
}

void AssignmentStatementTail::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<AssignmentStatementTail>(*this));
}

std::string AssignmentStatementTail::getId() const {
  return id;
}

SExpression AssignmentStatementTail::getExp() const {
  return exp;
}

/*=======================
 * While Statement 
 *=======================
 */
WhileStatement::WhileStatement(SStatementList statements, SIntExpression exp, unsigned int line) : Statement(line){
  this->statements = statements; 
  this->exp = exp;
}

void WhileStatement::print(std::ostream& output, int tabs) {
  printTabs(output, tabs);
  output << "WHILE" << "(";
  exp->print(output, tabs);
  output << ")" << std::endl;
  statements->print(output, tabs + 1);
}

void WhileStatement::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<WhileStatement>(*this)); 
}

SStatementList WhileStatement::getStatements() const {
  return statements;
}

SIntExpression WhileStatement::getExp() const {
  return exp;
}

/*=======================
 * If Statement 
 *=======================
 */
IfStatement::IfStatement(SIntExpression exp, SStatementList statements, SElseStatement elseStmt, unsigned int line): Statement(line) {
  this->exp = exp; 
  this->statements = statements;
  this->elseStmt = elseStmt;
}

void IfStatement::print(std::ostream& output, int tabs) {
  printTabs(output, tabs);
  output << "IF (";
  exp->print(output, tabs);
  output << ")";
  statements->print(output, tabs + 1);
  
  if (elseStmt) {
    elseStmt->print(output, tabs);
  }
}

void IfStatement::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<IfStatement>(*this));
}

SIntExpression IfStatement::getExp() const {
  return exp;
}

SElseStatement IfStatement::getElseStmt() const {
  return elseStmt;
}

SStatementList IfStatement::getStatements() const {
  return statements;
}

/*=======================
 * Else Statement 
 *=======================
 */
ElseStatement::ElseStatement(SStatementList statements, unsigned int line) : Statement(line) {
  this->statements = statements;
}

void ElseStatement::print(std::ostream& output, int tabs) {
  printTabs(output, tabs);
  output << "ELSE " << std::endl;
  statements->print(output, tabs + 1);
}

void ElseStatement::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<ElseStatement>(*this));
}

SStatementList ElseStatement::getStatements() const {
  return statements;
}

/*=======================
 * Return Statement 
 *=======================
 */

ReturnStatement::ReturnStatement(SExpression exp, unsigned int line) : Statement(line) {
  this->exp = exp;
}

void ReturnStatement::print(std::ostream& output, int tabs) {
  printTabs(output, tabs);
  output << "RETURN ";
  exp->print(output, tabs);
  output << std::endl;
}

void ReturnStatement::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<ReturnStatement>(*this));
}

SExpression ReturnStatement::getExp() const {
  return exp;
}

/*=======================
 * Function Call Statement 
 *=======================
 */
FunctionCallStatement::FunctionCallStatement(SIntFunctionCall functionCall, unsigned int line) : Statement(line) {
 this->functionCall = functionCall;
}

SIntFunctionCall FunctionCallStatement::getFunctionCall() const {
  return functionCall;
}

void FunctionCallStatement::print(std::ostream& output, int tabs) {
  getFunctionCall()->print(output, tabs);
}
	
void FunctionCallStatement::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<FunctionCallStatement>(*this));
}

/*=======================
 * Type 
 *=======================
 */

Type::Type(TOKEN_TYPE type, bool hasPointer, unsigned int line) : ASTNode(line){
  this->type = type;
  this->hasPointer = hasPointer;
}

Type::Type(TOKEN_TYPE type, bool hasPointer) : ASTNode(0){
  this->type = type;
  this->hasPointer = hasPointer;
}

void Type::print(std::ostream& output, int tabs) {
  output << Token::getNameFromType(type) << " ";

  if (hasPointer) {
    output << "* ";
  }
}

void Type::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<Type>(*this));
}

TOKEN_TYPE Type::getType() const {
  return type;
}

bool Type::isPointer() const {
  return hasPointer;
}

bool Type::equal(SType other) {
  return ((type == other->getType()) && (hasPointer == other->isPointer()));
}

/*=======================
 * Parameter List
 *=======================
 */

ParameterList::ParameterList(unsigned int line) : ASTNode(line) 
{
}

void ParameterList::print(std::ostream& output, int tabs) {
  for (unsigned int i = 0; i < parameters.size(); i++) {
    auto param = parameters[i];
    
    if (param) {
      param->print(output, tabs);

      if (i != (parameters.size() - 1)) {
        output << ", ";
      }
    }
  }
}

void ParameterList::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<ParameterList>(*this));
}

void ParameterList::addParameter(SParameter param) {
  parameters.push_back(param);
}

std::vector<SParameter> ParameterList::getParameters() const {
  return parameters;
}

/*=======================
 * Parameter 
 *=======================
 */

Parameter::Parameter(SType type, std::string id, unsigned int line) : ASTNode(line) {
  this->type = type;
  this->id = id;
}

void Parameter::print(std::ostream& output, int tabs) {
  type->print(output, tabs);
  output << id;
}

void Parameter::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<Parameter>(*this));
}

SType Parameter::getType() const {
  return type;
}

std::string Parameter::getId() const {
  return id;
}


/*=======================
 * Char literal Expression 
 *=======================
 */

CharLiteral::CharLiteral(char literal, unsigned int line) : Expression(line){
  this->literal = literal;
}

void CharLiteral::print(std::ostream& output, int tabs) {
  output << "'" << literal << "'";
}

void CharLiteral::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<CharLiteral>(*this));
}

/*=======================
 * String literal Expression 
 *=======================
 */

StringLiteral::StringLiteral(std::string literal, unsigned int line) : Expression(line){
  this->literal = literal;
}

void StringLiteral::print(std::ostream& output, int tabs) {
  output << "\"" << literal << "\"";
}

void StringLiteral::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<StringLiteral>(*this));
}

/*=======================
 * Int literal Expression 
 *=======================
 */

IntLiteral::IntLiteral(int literal, unsigned int line) : IntExpression(line) {
  this->literal = literal;
}

void IntLiteral::print(std::ostream& output, int tabs) {
    output << literal;
}

void IntLiteral::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<IntLiteral>(*this));
}


/*=======================
 * Int id Expression 
 *=======================
 */

IntId::IntId(std::string id, unsigned int line) : IntExpression(line) {
  this->id = id;
}

void IntId::print(std::ostream& output, int tabs) {
  output << id;
}

void IntId::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<IntId>(*this));
}

std::string IntId::getId() const {
  return id;
}

/*=======================
 * Int function call Expression 
 *=======================
 */

IntFunctionCall::IntFunctionCall(std::string functionId, SArguments args, unsigned int line) : IntExpression(line) {
  this->functionId = functionId;
  this->args = args;
}

void IntFunctionCall::print(std::ostream& output, int tabs) {
  output << functionId << "(";
  if (args) {
    args->print(output, tabs);
  }
  output << ")";
}

void IntFunctionCall::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<IntFunctionCall>(*this));
}

std::string IntFunctionCall::getId() const {
  return functionId;
}

SArguments IntFunctionCall::getArgs() const {
  return args;
}

/*=======================
 * Complex Expression 
 *=======================
 */
ComplexExpression::ComplexExpression(TOKEN_TYPE op, unsigned int line) : IntExpression(line) {
  this->op = op;
  this->lhs = nullptr;
  this->rhs = nullptr;
}

void ComplexExpression::setLHS(SExpression lhs) {
  this->lhs = lhs;
}

void ComplexExpression::setRHS(SExpression rhs) {
  this->rhs = rhs;
}

void ComplexExpression::print(std::ostream& output, int tabs) {
  if (lhs) {
    lhs->print(output, tabs);
  }
  output << " " << Token::getNameFromType(op) << " ";
  if (rhs) {
    rhs->print(output, tabs);
  }
}

void ComplexExpression::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<ComplexExpression>(*this));
}

SExpression ComplexExpression::getLHS() const {
  return lhs;
}  

SExpression ComplexExpression::getRHS() const {
  return rhs;
}

/*=======================
 * Not Expression 
 *=======================
 */
NotExpression::NotExpression(SIntExpression exp, unsigned int line) : IntExpression(line) {
  this->exp = exp;
}

void NotExpression::print(std::ostream& output, int tabs) {
  output << "NOT ";
  exp->print(output, tabs);
}

void NotExpression::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<NotExpression>(*this));
}

SIntExpression NotExpression::getExp() const {
  return exp;
}

/*=======================
 * Arguments 
 *=======================
 */

std::vector<SExpression> Arguments::getArguments() const {
  return args;
}

void Arguments::addArgument(SExpression arg) {
  args.push_back(arg);
}

void Arguments::print(std::ostream& output, int tabs) {
  for (unsigned int i = 0; i < args.size(); i++) {
    auto exp = args[i];
    if (i > 0) {
      output << ", ";
    }
    exp->print(output, tabs);
  }
}

void Arguments::accept(Visitor* visitor) {
  visitor->visit(std::make_shared<Arguments>(*this));
}
