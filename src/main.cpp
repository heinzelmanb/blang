#include <iostream>
#include <fstream> 
#include <string>

#include "lexer.h"
#include "parser.h"
#include "typecheck.h"
#include "blang_exception.h"

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "Please input a source file; Exit 1" << std::endl;
    return 1;
  }
  std::string file(argv[1]);

  std::ifstream program(file, std::ios::binary);

  if (program.fail()) {
    std::cerr << "The file: " << file << " does not exist; Exit 1" << std::endl;
    return 1;
  }

  auto lex = std::make_shared<Lexer>(Lexer(&program));
  auto parser = Parser(lex);

  ast::SProgram ast = parser.parse(std::cerr);
  
  if (ast) {
    auto symbols = std::make_shared<SymbolTable>();
    TypeCheck tc = TypeCheck(ast, symbols);
    
    tc.typeCheck(std::cerr); 
  }

  return 0;
}
